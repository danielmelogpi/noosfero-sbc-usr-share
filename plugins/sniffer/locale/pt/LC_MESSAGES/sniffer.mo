��            +         �     �     �     �  
   �                 	   :  !   D     f  	   �     �  #   �     �     �  
   �     �     �  �     ?   �  	   >  8   H     �     �  	   �     �     �     �  	   �     �  n  �     d     ~     �     �     �     �  !   �     �  '        ,  
   L     W  &   _     �     �     �     �     �  �   �  N   �	     �	  ,   	
     6
     P
     V
     c
     y
     �
     �
     �
                                 	                                                              
                                                 %{interest} from %{profile} Back to control panel Buyer interests Can supply Close Consumer Interests Consumer interests updated Consumers Could not save consumer interests Edit %{inputs} and %{interests} Interests Legend Lists declared and inputs interests Lists interests Maximum distance: May consum Opportunities Sniffer Save Select here products and services categories that you have an interest on buying. Then you can go to the Opportunity Sniffer and check out enterprises near you that offer such products. Type in some characters and choose your interests from our list. Sniffs product suppliers and consumers near to your enterprise. Suppliers This block show interests of your profile or environment Type in a keyword both consumers declared interests look for products... products' inputs suppliers your enterprise Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-05-07 11:11-0300
Last-Translator: daniel tygel <dtygel@eita.org.br>
Language-Team: pt_BR <dtygel@eita.org.br>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;
 %{interest} de %{profile} Voltar ao painel de controle Interesses de compra Fornece Fechar Interesses de consumo Interesses de consumo atualizados Consumidores Não foi possível salvar seu interesse Editar %{inputs} e %{interests} Interesses Legenda Mostra insumos e interesses de consumo Mostra interesses Distância máxima: Consome Farejador de oportunidades Salvar Selecione aqui as categorias de produtos e serviços que você tem interesse em comprar. Com isso, empreendimentos saberão suas necessidades e poderão te fazer ofertas.Digite e escolha quais são seus interesses. Busca fornecedores e consumidores de produtos próximos ao seu empreendimento. Fornecedores Este bloco mostra seus interesses ou da rede Escreva uma palavra chave ambos consumidores interesses declarados buscar por produtos... Insumos de produtos fornecedores seu empreendimento 