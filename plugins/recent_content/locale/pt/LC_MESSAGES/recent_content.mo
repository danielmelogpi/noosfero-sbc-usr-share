��          �      �       H  R   I  '   �  *   �  %   �          .  C   ;  	     �   �  N   "     q  
   �     �  �  �  `   U  )   �  *   �  #        /     N  N   a  	   �  �   �  b   J     �     �     �              	                                           
        A plugin that adds a block where you can display the content of any of your blogs. Choose how many items will be displayed Choose how the content should be displayed Choose which blog should be displayed Display blog cover image Full content No blogs found. Please add a blog in order to configure this block. Read more This block displays all articles inside the blog you choose. You can edit the block to select which of your blogs is going to be displayed in the block. This is the recent content block. Please edit it to show the content you want. Title and abstract Title only View All Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-23 11:37+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-recent-content/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Um plugin que adiciona blocos onde você pode mostrar o conteúdo de qualquer um dos seus blogs. Escolha quantos itens devem ser mostrados Escolha como o conteúdo deve ser mostrado Escolha qual blog deve ser mostrado Mostrar imagem de capa do blog Conteúdo completo Nenhum blog encontrado. Por favor adicione um blog para configurar esse bloco. Leia mais Esse bloco apresenta todos os conteúdos do blog escolhido. Você pode editar esse bloco para escolher qual dos seus blogs será mostrado nele. Esse é o bloco de conteúdo recente. Por favor edite-o para mostrar o conteúdo que você deseja. Título e resumo Somente título Ver Todos(as) 