��    *      l  ;   �      �     �     �  N   �     -  /   ?     o     w     �  ,   �  ,   �     �     �          )     ;     N     b     j          �     �     �     �     �     �     �     	          5     L     c     h     x     �     �     �     �     �     �     �     �  �    +   �  (   		  S   2	     �	  -   �	     �	     �	     �	  5   �	  3   5
     i
  )   o
     �
     �
     �
     �
  
   �
               7     @      _     �     �     �     �     �  *   �  (   	  &   2     Y     `     u     }     �     �     �     �     �     �                #                '               (   *                                           &             %                                               "      )   $   
   	                      !             (no label registered yet) (no status registered yet) <i>%{user}</i> added the status <i>%{status_name}</i> at <i>%{created_at}</i>. <i>Reason:</i> %s A plugin that allow classification of comments. Actions Add a new label Add a new status Are you sure you want to  remove this label? Are you sure you want to remove this status? Color Comments classification options Editing label %s Editing status %s Enable this label? Enable this status? Enabled Failed to edit label Failed to edit status Label Label could not be created Label could not be removed Label created Label removed Label updated Manage Labels Manage Status Manage comment classification Manage comments labels Manage comments status Name Reason enabled? Reason: Status Status could not be created Status could not be removed Status created Status for comment Status removed Status updated [Select ...] Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-24 20:46+0200
Last-Translator: Gonzalo Exequiel Pedone <hipersayan.x@gmail.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/plugin-comment-classification/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 (todavía no se ha registrado una etiqueta) (todavía no se ha registrado un estado) <i>%{user}</i> ha agregado el estado <i>%{status_name}</i> en <i>%{created_at}</i>. <i>Razón:</i> %s Un plugin que permite clasificar comentarios. Acciones Agregar una nueva etiqueta Agregar nuevo estado ¿Estás seguro de que deseas eliminar esta etiqueta? ¿Estás seguro de que deseas eliminar este estado? Color Opciones de clasificación de comentarios Editar etiqueta %s Editando el estado %s ¿Habilitar esta etiqueta? ¿Habilitar este estado? Habilitado Fallo al editar la etiqueta Fallo al editar el estado Etiqueta La etiqueta no pudo ser creada No se puede eliminar la etiqueta Etiqueta creada Etiqueta eliminada Etiqueta actualizada Gestionar Etiquetas Gestionar Estados Gestionar la clasificación de comentarios Gestionar la etiqueta de los comentarios Gestionar el estado de los comentarios Nombre ¿Razón habilitada? Razón: Estado No se puede crear el estado No se puede eliminar el estado Estado creado Estado del comentario Estado eliminado Estado actualizado [Seleccione ...] 