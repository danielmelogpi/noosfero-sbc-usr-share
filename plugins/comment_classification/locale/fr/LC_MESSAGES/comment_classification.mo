��    *      l  ;   �      �     �     �  N   �     -  /   ?     o     w     �  ,   �  ,   �     �     �          )     ;     N     b     j          �     �     �     �     �     �     �     	          5     L     c     h     x     �     �     �     �     �     �     �     �  �    .   �  3   	  V   J	     �	  ,   �	     �	     �	     
  5   
  3   Q
     �
  %   �
     �
     �
     �
     �
  
     "     "   3     V  !   \  $   ~     �     �     �     �     �  #   �  "     #   =     a     e     w     �  '   �  ,   �     �     �           (     A           #                '               (   *                                           &             %                                               "      )   $   
   	                      !             (no label registered yet) (no status registered yet) <i>%{user}</i> added the status <i>%{status_name}</i> at <i>%{created_at}</i>. <i>Reason:</i> %s A plugin that allow classification of comments. Actions Add a new label Add a new status Are you sure you want to  remove this label? Are you sure you want to remove this status? Color Comments classification options Editing label %s Editing status %s Enable this label? Enable this status? Enabled Failed to edit label Failed to edit status Label Label could not be created Label could not be removed Label created Label removed Label updated Manage Labels Manage Status Manage comment classification Manage comments labels Manage comments status Name Reason enabled? Reason: Status Status could not be created Status could not be removed Status created Status for comment Status removed Status updated [Select ...] Project-Id-Version: 1.2~rc2-23-g29aba34
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-07-03 00:34+0200
Last-Translator: Christophe DANIEL <papaeng@gmail.com>
Language-Team: French <https://hosted.weblate.org/projects/noosfero/plugin-comment-classification/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.4-dev
 (pour le moment aucun label n'est enregistré) (il n'y a pas de statut enregistré pour le moment) <i>%{utilisateur}</i> ajout des statuts <i>%{nom_ du_statut}</i> à <i>%{creer_a}</i>. <i>Raison:</i> %s Un greffon qui permet de ranger du contenue. Actions Ajouter un nouveau label Ajouter un nouveau statut Êtes-vous sûr(e) que vous voulez retirer ce label ? Êtes-vous sûr(e) de vouloir supprimer ce statut ? Couleur Options de rangement des commentaires Édition du label %s Édition du statut %s Activer ce label ? Activer ce statut ? Activé(e) Erreur lors de l'édition du label Échec dans l'édition des statuts Label Le label n'a pas pu êtres créer Le label n'a pas pu êtres supprimé Label crée Label supprimé Label mis à jour Gérer les labels Gérer les statuts Manager le rangement de commentaire Gérer les commentaires des labels Gérer les commentaires des statuts Nom Raison activée ? Raison : Statuts Les statuts n'ont pas pu êtres créés Les statuts ne peuvent pas êtres supprimés Statut créé Statuts pour le commentaire Les statuts ont été supprimés Les statuts sont à jour [Choisir...] 