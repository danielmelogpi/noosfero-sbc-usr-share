��    *      l  ;   �      �     �     �  N   �     -  /   ?     o     w     �  ,   �  ,   �     �     �          )     ;     N     b     j          �     �     �     �     �     �     �     	          5     L     c     h     x     �     �     �     �     �     �     �     �  �    "   �      �  Q   	     g	  8   y	     �	     �	     �	  +   �	  ,   
     D
  +   H
     t
     �
     �
     �
  
   �
     �
     �
               5     U     e     w     �     �  (   �  $   �      �           %     8     @     G     d     �     �     �     �     �           #                '               (   *                                           &             %                                               "      )   $   
   	                      !             (no label registered yet) (no status registered yet) <i>%{user}</i> added the status <i>%{status_name}</i> at <i>%{created_at}</i>. <i>Reason:</i> %s A plugin that allow classification of comments. Actions Add a new label Add a new status Are you sure you want to  remove this label? Are you sure you want to remove this status? Color Comments classification options Editing label %s Editing status %s Enable this label? Enable this status? Enabled Failed to edit label Failed to edit status Label Label could not be created Label could not be removed Label created Label removed Label updated Manage Labels Manage Status Manage comment classification Manage comments labels Manage comments status Name Reason enabled? Reason: Status Status could not be created Status could not be removed Status created Status for comment Status removed Status updated [Select ...] Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-23 11:38+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-comment-classification/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 (nenhum marcador registrado ainda) (nenhum status registrado ainda) <i>%{user}</i> adicionou o status <i>%{status_name}</i> às <i>%{created_at}</i>. <i>Razão:</i> %s Um plugin que permite a classificação de comentários. Ações Adicionar um novo marcador Adicionar novo status Tem certeza que quer excluir este marcador? Tem certeza de que quer excluir este status? Cor Opções de classificação de comentários Editando marcador %s Editando status %s Habilitar esse marcador? Habilitar esse status? Habilitado Falhou em editar o marcador Falhou em editar o status Marcador Marcador não pôde ser criado Marcador não pode ser removido Marcador criado Marcador removido Marcador atualizado Gerenciar Marcadores Gerenciar Status Gerenciar classificação de comentário Gerenciar marcadores de comentários Gerenciar status de comentários Nome Razão habilitada? Razão: Estado Status não pôde ser criado Status não pôde ser removido Status criado Status para comentário Status removido Status atualizado [Selecione ...] 