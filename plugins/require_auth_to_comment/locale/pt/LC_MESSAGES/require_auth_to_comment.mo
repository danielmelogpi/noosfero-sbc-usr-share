��          D      l       �   *   �      �   9   �      �   �  �   3   �       B        R                          Accept comments from unauthenticated users Comments Requires users to authenticate in order to post comments. Save Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-08-07 16:48+0200
Last-Translator: Antonio Terceiro <terceiro@softwarelivre.org>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-require-auth-to-comment/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.4-dev
 Aceitar comentários de usuários não autenticados Comentários Exige que os usuários se autentiquem antes de postar coentários. Salvar 