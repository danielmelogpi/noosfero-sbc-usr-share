��          <      \       x   *   y      �   9   �   �  �   /   �     �  G   �                           Accept comments from unauthenticated users Comments Requires users to authenticate in order to post comments. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-11-03 15:52+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/noosfero/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0-dev
 Aceptar comentarios de usuarios no autenticados Comentarios Se requiere que el usuario se autentique para poder enviar comentarios. 