��          <      \       x   *   y      �   9   �   �  �   4   �  
   �  ?   �                           Accept comments from unauthenticated users Comments Requires users to authenticate in order to post comments. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 Kommentare von nicht angemeldeten Nutzer akzeptieren Kommentare Nutzer müssen sich anmelden, um Kommentare abgeben zu dürfen. 