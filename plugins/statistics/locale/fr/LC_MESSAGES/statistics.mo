��          �      L      �  H   �     
           5  -   L     z     �     �     �     �     �  7   �  
   %     0     <     H     M     V  �  \  S          t  !   �  !   �  4   �          .     N     l      �     �  @   �                 
   #     .     7            	                                                
                                        A plugin that adds a block where you can see statistics of it's context. Show category counter Show comment counter Show community counter Show counter for communities with template %s Show enterprise counter Show hit counter Show product counter Show tag counter Show user counter Statistics for %s This block presents some statistics about your context. categories communities enterprises hits products users Project-Id-Version: 1.2~rc2-23-g29aba34
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-03-07 02:46+0200
Last-Translator: Tuux <tuxa@galaxie.eu.org>
Language-Team: French <https://hosted.weblate.org/projects/noosfero/plugin-statistics/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.3-dev
 Un greffon qui ajoute un bloc ou vous pouvez voir les statistiques de son contexte. Montrer le nombre de catégories Montrer le nombre de commentaires Montrer le nombre de communautés Montrer le nombre de communautés avec un modèle %s Montrer le nombre d'entreprises Montrer le nombre de rencontres Montrer le nombre de produits Montrer le nombre d'étiquettes Montrer le nombre d'utilisateurs Statistiques pour %s Ce bloc présente quelques statistiques sur votre environnement. catégories groupes entreprises rencontres produits utilisateurs 