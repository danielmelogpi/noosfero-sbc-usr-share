��          �      L      �  H   �     
           5  -   L     z     �     �     �     �     �  7   �  
   %     0     <     H     M     V  �  \  P         `  "   �  !   �  -   �  1   �     &     D     c     ~     �  <   �  
   �     �                  	   )            	                                                
                                        A plugin that adds a block where you can see statistics of it's context. Show category counter Show comment counter Show community counter Show counter for communities with template %s Show enterprise counter Show hit counter Show product counter Show tag counter Show user counter Statistics for %s This block presents some statistics about your context. categories communities enterprises hits products users Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-03-12 15:11+0200
Last-Translator: daniel <dtygel@eita.org.br>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-statistics/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Um plugin que adiciona um bloco onde você pode ver estatísticas do seu portal. Mostrar quantidade de categorias Mostrar quantidade de comentários Mostrar quantidade de comunidades Mostrar quantidade de comunidades com tema %s Mostrar quantidade de empreendimentos solidários Mostrar quantidade de acessos Mostrar quantidade de produtos Mostrar quantidade de tags Mostrar quantidade de usuários Estatísticas para %s Este bloco apresenta algumas estatísticas sobre seu portal. categorias comunidades empreendimentos acessos produtos usuários 