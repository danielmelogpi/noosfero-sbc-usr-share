��          �            x  H   y     �     �     �  -        2     J     [     l     ~  7   �  
   �     �     �     �  �  �  Q   �                 @  <   a     �     �     �     �       <   )     f     r     {     �                                     	                                      
    A plugin that adds a block where you can see statistics of it's context. Show category counter Show comment counter Show community counter Show counter for communities with template %s Show enterprise counter Show hit counter Show tag counter Show user counter Statistics for %s This block presents some statistics about your context. categories enterprises hits users Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-24 19:06+0200
Last-Translator: Gonzalo Exequiel Pedone <hipersayan.x@gmail.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/plugin-statistics/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Un plugin que añade un bloque donde puedes ver las estadísticas de su contexto. Mostrar contador de categoría Mostrar contador de comentarios Mostrar contador de la comunidad Mostrar el contador para las comunidades con la plantilla %s Mostrar contador empresarial Mostrar contador de aciertos Mostrar contador de etiquetas Mostrar contador de usuarios Estadísticas para %s Este bloque presenta algunas estadísticas sobre su entorno. categorías empresas aciertos usuarios 