��    /      �  C           7     3   Q     �     �     �     �  x   �     -  +   4  *   `  ,   �     �     �     �  	   �     �     
     $     >     K      \     }     �     �     �  	   �     �     �     �     �     �     �     �               ,     =     I  '   \  )   �     �     �     �     �     �     �  �  �  C   �	  D   �	     
     #
     ,
     G
  {   _
     �
  9   �
  =     :   Y     �     �     �     �  *   �  )     -   <     j     |  1   �  #   �     �     �     	          +     I     N     ]     `  	   o     y     �  +   �     �  	   �     �  2   �  6        V     [     `     g     n     s                   "                              .       *             #   
                                               '          	              !                   +             ,       %   )   $   /          &                 (   -    %{requestor} wants to fill in some further information. %{requestor} wants you to fill in some information. Access Actions Add a new form Add a new option After joining %{requestor}, the administrators of this organization
      wants you to fill in some further information. Always Are you sure you want to remove this field? Are you sure you want to remove this form? Are you sure you want to remove this option? Back to forms Back to submissions Custom Edit form Enables the creation of forms. Form could not be removed Form could not be updated Form removed From %s until %s Invalid string format of access. Invalid type format of access. Logged users Manage Forms Manage forms Mandatory Membership survey Name New form Ok Pending Period Select Sort by Submission could not be saved Submission saved Submissions Submissions for %s There are no submissions for this form. There is no profile with the provided id. Time Type: Until %s User Value is mandatory. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-03-09 09:51+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/plugin-custom-forms/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 %{requestor} möchte von Ihnen, dass Sie weitere Felder ausfüllen. %{requestor} möchte von Ihnen, dass Sie ein paar Felder ausfüllen. Zugriff Aktionen Neues Formular hinzufügen Neue Option hinzufügen Nach dem Beitritt zu %{requestor} möchte der Administrator der Organisation,
      dass Sie noch weitere Angaben tätigen. Immer Sind Sie sicher, dass Sie dieses Feld entfernen möchten? Sind Sie sicher, dass Sie dieses Formular entfernen möchten? Sind Sie sicher, dass Sie diese Option entfernen möchten? Zurück zu den Formularen Zurück zu den Beiträgen Individuell Formular editieren Ermöglicht die Erstellung von Formularen. Das Formular konnte nicht entfernt werden Das Formular konnte nicht aktualisiert werden Formular entfernt Von %s bis %s Ungültiges Zeichenkettenformat für den Zugriff. Ungültiges Typformat beim Zugriff. Angemeldete Benutzer Formulare verwalten Formulare verwalten Pflichtangabe Umfrage unter den Mitgliedern Name Neues Formular OK In Bearbeitung Intervall Wählen Sortieren nach Der Beitrag konnte nicht gespeichert werden Beitrag gespeichert Beiträge Beiträge für %s Es gibt keine Unterbeiträge für dieses Formular. Es gibt kein Profil mit dem angegebenen Identifikator. Zeit Typ: Bis %s Nutzer Wert ist zwingend notwendig 