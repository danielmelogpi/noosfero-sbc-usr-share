��          �            h  ]   i     �     �          "     =     X     t     �     �     �     �     �  *   �  �    c   �     ;      [      |     �     �     �     �          (     >     Q     f  .   z           	   
                                                                  A plugin that lists the most accessed, most commented, most liked and most disliked contents. Display most accessed content Display most commented content Display most disliked content Display most liked content Display most voted content Limit of items per category Most commented articles Most disliked articles Most liked articles Most read articles Most voted articles Relevant content This block lists the most popular content. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Um plugin que lista os conteúdos mais acessados, mais comentados, mais curtidos e mais rejeitados. Mostrar conteúdo mais acessado Mostrar conteúdo mais comentado Mostrar conteúdo mais rejeitado Mostrar conteúdo mais curtido Mostrar conteúdo mais votado Limite de itens por categoria Artigos mais comentados Artigos mais rejeitados Artigos mais curtidos Artigos mais lidos Artigos mais votados Conteúdo relevante Este bloco apresenta o conteúdo mais popular. 