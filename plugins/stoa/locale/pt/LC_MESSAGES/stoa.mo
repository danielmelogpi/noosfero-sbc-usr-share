��          �      ,      �     �     �     �     �     �     �  A   �  D   <     �  ]   �  q   �     o  
   �  
   �     �  -   �  �  �     �  &   �     �     �             E   7  H   }  %   �  f   �  }   S     �     �     �       7   '                                     
                                     	     validation failed Add Stoa features Birth date (yyyy-mm-dd) CPF Checking usp number... Confirm your CPF number. Confirm your birth date. Pay attention to the format: yyyy-mm-dd. Either this usp number is being used by another user or is not valid Incorrect user/password pair. The usp id grants you special powers in the network. Don't forget to fill it if you have one. The usp id grants you special powers in the network. Don't forget to fill it with a valid number if you have one. This field must be private USP Number USP number USP number / Username is being used by another user or is not valid Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
  validação falhou Adiciona a funcionalidades para o Stoa Data de nascimento (aaaa-mm-dd) CPF Checando o número usp... Confirme seu número de CPF. Confirme sua data de nascimento. Fique atento ao formato: aaaa-mm-dd. Esse número usp está sendo usado por outro usuário ou não é válido O par usuário/senha está incorreto. O número USP garante poderes especiais nesta rede. Não esqueça de preenchê-lo se você possuir um. O número USP garante poderes especiais nesta rede. Não esqueça de preenchê-lo com um número válido se você possuir um. Este campo precisa ser privado Número USP Número USP Número USP / Nome de usuário está sendo usado por outro usuário ou não é válido 