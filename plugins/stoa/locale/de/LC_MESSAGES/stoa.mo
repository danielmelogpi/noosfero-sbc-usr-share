��          �            x     y     �     �     �     �     �  A   �  D   ,     q  ]   �  q   �     _  
   z     �  -   �  �  �     u     �     �     �     �      �  L     W   S  *   �  �   �  �   ^     �  
          C   '                                      
                                     	     validation failed Add Stoa features Birth date (yyyy-mm-dd) CPF Checking usp number... Confirm your CPF number. Confirm your birth date. Pay attention to the format: yyyy-mm-dd. Either this usp number is being used by another user or is not valid Incorrect user/password pair. The usp id grants you special powers in the network. Don't forget to fill it if you have one. The usp id grants you special powers in the network. Don't forget to fill it with a valid number if you have one. This field must be private USP number USP number / Username is being used by another user or is not valid Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
  Validierung fehlgeschlagen Fügt die Stoa-Funktion hinzu Geburtstag (jjjj-mm-tt) CPF Überprüfe USP-Nummer... Bestätigen Sie Ihre CPF-Nummer. Bestätigen Sie Ihr Geburtsdatum. Folgen Sie dabei diesem Format: jjjj-mm-tt Die USP-Nummer wird entweder bereits von einem anderen Nutzer belegt oder ist ungültig Falsches Paar aus Nutzername und Passwort. Die USP-Nummer gibt Ihnen spezielle Kräfte im Netzwerk. Vergessen Sie nicht, sie als gültige Nummer anzugeben, sofern Sie eine haben. Die USP-Nummer gibt Ihnen spezielle Kräfte im Netzwerk. Vergessen Sie nicht, sie als gültige Nummer anzugeben, sofern Sie eine haben. Dieses Feld muss privat sein USP-Nummer USP-Nummer/Telefonnummer wird bereits von einem anderen Nutzer eingesetzt oder ist ungültig 