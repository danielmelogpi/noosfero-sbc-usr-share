��          T      �       �      �      �   C   �        5   %  @   [  �  �     \     u  B   �     �  M   �  R                                            Invalid lattes url  is invalid. A plugin that imports the lattes curriculum into the users profiles Lattes Lattes Platform is unreachable. Please, try it later. Lattes not found. Please, make sure the informed URL is correct. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-17 10:50+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-lattes-curriculum/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
  URL do Lattes inválida  é inválido. Um plugins que importa o currículo Lattes no perfil dos usuários Lattes A Plataforma Lattes está inacessível. Por favor tente novamente mais tarde. Lattes não encontrado. Por favor certifique-se que a URL informada está correta. 