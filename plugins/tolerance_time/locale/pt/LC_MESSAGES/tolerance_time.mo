��          �      �       8  ?   9     y     �  0   �     �     �     �  J   �     I     `     v     �  �  �  R   d  2   �  /   �  9        T     Z     b  Q   k     �     �  &   �     "                  
                                            	    Adds a tolerance time for editing content after its publication Comment edition tolerance time Content edition tolerance time Empty means unlimited and zero means right away. Hours Minutes Seconds This content can't be edited anymore because it expired the tolerance time Tolerance Adjustements Tolerance Adjustments Tolerance could not be updated Tolerance updated Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Adiciona um tempo de tolerância para edição de conteúdo após sua publicação Tempo de tolerância para edição de comentários Tempo de tolerância para edição de conteúdo Vazio significa ilimitado e zero significa imediatamente. Horas Minutos Segundos Este conteúdo não pode mais ser editado porque seu tempo de tolerância expirou Configurações de Tolerância Configurações de Tolerância A tolerância não pode ser atualizada A tolerância foi atualizada 