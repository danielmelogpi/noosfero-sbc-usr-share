��          �      �       8  ?   9     y     �  0   �     �     �     �  J   �     I     `     v     �  �  �  l   S  $   �  #   �  ;   	     E     M     U  Q   ^     �     �  -   �                       
                                            	    Adds a tolerance time for editing content after its publication Comment edition tolerance time Content edition tolerance time Empty means unlimited and zero means right away. Hours Minutes Seconds This content can't be edited anymore because it expired the tolerance time Tolerance Adjustements Tolerance Adjustments Tolerance could not be updated Tolerance updated Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 Fügt eine Toleranzzeit hinzu, während derer der Beitrag nach dessen Veröffentlichung editiert werden darf Toleranzzeit zur Kommentareditierung Toleranzzeit zur Beitragseditierung Leer heißt unbegrenzt und Null heißt direkt im Anschluss. Stunden Minuten Sekunden Der Beitrag kann nicht weiter editiert werden, da die Toleranzzeit abgelaufen ist Toleranzeinstellung Toleranzanpassungen Die Tolernaz konnte nicht aktualisiert werden Toleranz aktualisiert 