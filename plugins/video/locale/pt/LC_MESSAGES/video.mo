��          D      l       �   R   �      �   2   �   _     �    \   <     �  <   �  b   �                          A plugin that adds a block where you can add videos from youtube, vimeo and html5. Display a Video Register a valid url (Vimeo, Youtube, video files) This block presents a video from youtube, vimeo and some video formats (mp4, ogg, ogv and webm) Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Um plugin que adiciona um bloco onde você pode adicionar vídeos do youtube, vimeo e html5. Mostrar um Vídeo Registre uma url válida (Vimeo, Youtube, arquivo de vídeo) Esse bloco apresenta um vídeo do youtube, vimeo e alguns formatos de video (mp4, ogg, ogv e webm) 