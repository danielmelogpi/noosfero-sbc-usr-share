��    (      \  ;   �      �  '   �     �     �     �     �     �  0   �      !  !   B  
   d     o     v  /   �     �     �  6   �  (   .     W     \     u     �     �     �     �      �  !   �          #     <  `   Q     �     �     �     �     �  +     <   ;     x     �  c  �  &   	     3	     ;	     B	     I	     f	  4   z	  *   �	  *   �	  	   
     
  $   
  =   <
  <   z
     �
  0   �
  &   �
     !     (     G     b     r     �     �  &   �      �               ,  q   D     �     �     �     �     �  (     9   D     ~  '   �        %   #              (                           	       $   '                            
                                !                              "      &                                          A plugin for environment notifications. Actions Activate Back Back to control panel Blue - Information Could not change the status of the notification. Could not edit the notification. Could not remove the notification Deactivate Delete Display only in the homepage Display popup until user close the notification Display to not logged users too Do not show anymore Do you want to change the status of this notification? Do you want to delete this notification? Edit Enter your message here: Environment Notifications Green - Success Hide for now New Notification Notification Manager Notification couldn't be created Notification successfully created Notifications Notifications Color/Type Notifications Status Obs: You can use %{name} and %{email} variables to put the user's name and email in the message. Optional Title: Red - Danger Save The notification was deleted. The notification was edited. The status of the notification was changed. There are active notifications in this environment! You can  Yellow - Warning manage all notifications here. Project-Id-Version: 1.2-514-gbe9e36b
POT-Creation-Date: 2015-09-24 18:52-0000
PO-Revision-Date: 2015-09-14 12:12-0000
Last-Translator: FULL NAME <arthurmde@gmail.com>
Language-Team: Portuguese
Language: Portuguese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;
 Plugin para notificações do ambiente Ações Ativar Voltar Voltar ao Painel de Controle Azul - Informação Não foi possível alterar o status da notificação Não foi possível editar a notificação. Não foi possível remover a notificação Desativar Remover Apresentar apenas na página inicial Apresentar Popup da notificação até que o usuário a feche Apresentar notificação para usuários não logados também Não mostrar mais Você quer alterar o status dessa notificação? Você quer remover essa notificação? Editar Entre com a sua mensagem aqui: Notificações do Ambiente Verde - Sucesso Ocultar momentaneamente Nova Notificação Gerenciador de Notificações Não foi possível criar notificação Notificação criada com sucesso Notificações Cor/Tipo de Notificação Status da Notificação Obs: Você pode usar as variáveis %{name} e %{email} para inserir o nome e email do usuário logado na mensagem. Título Opcional: Vermelho - Perigo Salvar A notificação foi removida A notificação foi editada. O status da notificação foi modificado Existem notificações ativas neste ambiente! Você pode  Amarelo - Atenção gerenciar todas as notificações aqui. 