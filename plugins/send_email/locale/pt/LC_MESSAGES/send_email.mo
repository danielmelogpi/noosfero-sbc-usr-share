��          �      �       8  8   9  !   r  ,   �  4   �  !   �          1  *   A     l     y     �     �  �  �  P   p  +   �  6   �  =   $  *   b     �     �  8   �     �  "         .     O                  	                              
                  '%s' address is not allowed (see SendEmailPlugin config) '%s' isn't a valid e-mail address (list of email addresses separated by comma) A plugin that allows sending e-mails via HTML forms. Configurations could not be saved Configurations was saved Contact from %s E-Mail addresses you want to allow to send Message sent SendEmailPlugin's config The message could not be sent To Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 %{fn} '%s' endereço não permitido (veja as configurações do SendEmailPlugin) '%s' não é um endereço de e-mail válido (lista de endereços de e-mail separadas por vírgula) Um plugin que permite envio de e-mails via formulários HTML. As configurações não puderam ser salvas As configurações foram salvas Contato de %s Endereços de e-mail que você deseja liberar para envio A mensagem foi enviada Configurações do SendEmailPlugin A mensagem não pode ser enviada Para 