��          �      �       8  8   9  !   r  ,   �  4   �  !   �          1  *   A     l     y     �     �  �  �  M   _  $   �  +   �  K   �  1   J  #   |     �  9   �     �  "   �  -        M                  	                              
                  '%s' address is not allowed (see SendEmailPlugin config) '%s' isn't a valid e-mail address (list of email addresses separated by comma) A plugin that allows sending e-mails via HTML forms. Configurations could not be saved Configurations was saved Contact from %s E-Mail addresses you want to allow to send Message sent SendEmailPlugin's config The message could not be sent To Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 Die Adresse von %s ist nicht erlaubt (siehe Einstellungen zu SendEmailPlugin) %s ist keine gültige E-Mail-Adresse (liste von kommagetrennten E-Mail-Adressen) Ein Plugin, welches die Zustellung von E-Mails von HTML-Formularen erlaubt. Die Konfiguration konnte nicht gespeichert werden Die Konfiguration wurde gespeichert Kontakt von %s E-Mail-Adressen, welche Sie zum Senden freigeben möchten Nachricht gesendet Konfiguration des SendEmailPlugins Die Nachricht konnte nicht abgeschickt werden An 