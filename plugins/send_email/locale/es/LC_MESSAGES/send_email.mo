��          �      �       8  8   9  !   r  ,   �  4   �  !   �          1  *   A     l     y     �     �  �  �  K   `  9   �  @   �  \   '  #   �     �     �  >   �       !   )     K     j                  	                              
                  '%s' address is not allowed (see SendEmailPlugin config) '%s' isn't a valid e-mail address (list of email addresses separated by comma) A plugin that allows sending e-mails via HTML forms. Configurations could not be saved Configurations was saved Contact from %s E-Mail addresses you want to allow to send Message sent SendEmailPlugin's config The message could not be sent To Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-11-03 15:52+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/noosfero/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0-dev
 La dirección '%s' no es permitida (ver configuración de SendEmailPlugin)  '%s' no es una dirección de correo electrónico válida. (lista de direcciones de correo electrónico separadas por coma) Una característica adicional que permite enviar correos electrónicos via formularios HTML. La configuración no pudo guardarse La configuración fue guardada Contactar desde %s Direcciones de correo electrónico que quieres permitir enviar Mensaje enviado Configuración de SendEmailPlugin El mensaje no pudo ser enviado Para 