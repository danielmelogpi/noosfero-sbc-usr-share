��          �      <      �  $   �  &   �  !   �          ,  M   3     �     �     �  /   �  *   �           8  2   T     �     �     �  `  �  6   #  2   Z  #   �     �     �  \   �     *     F     V  *   v  1   �     �  '   �  4        O     T     r                        	   
                                                                          (Don't check to show only %s events) (Don't check to show only your events) Duration: 1 day Duration: %s days Event Extras Events Include a new block to show the environment's or profiles' events information Limit of days to display Limit of items One month ago %d months ago One month left to start %d months left to start Only show events in this interval of days. Only show future events Show all environment events Show the profile events or all environment events. Today Tomorrow %d days left to start Yesterday %d days ago Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-01-30 00:18-0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;
 (Não marque para apresentar somente os eventos de %s) (Não marque para apresentar somente seus eventos) Duração: 1 dia Duração: %s dias Extras para Eventos Eventos Adiciona um novo bloco para apresentar as informações de eventos do ambiente ou dos perfis Limite de dias para mostrar Limite de itens Um mês atrás %d meses atrás. Um mês para iniciar %d meses para iniciar Mostar somente os eventos nesse intervalo de dias Mostrar apenas eventos futuros Apresentar todos os eventos do ambiente Mostra todos os eventos de um perfil ou do ambiente. Hoje Amanhã %d dias para começar Ontem %d dias atrás 