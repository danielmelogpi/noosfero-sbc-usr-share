��    '      T  5   �      `     a     o  4   �  A   �                         #  	   9  
   C  A   N  #   �     �  	   �     �  )   �  &   
  (   1  1   Z     �     �     �     �     �  z   �     x  !   �  )   �     �     �     �  k   �     W     u     �     �  &   �  �  �     t	  "   �	  <   �	  O   �	     ?
     E
     R
  	   _
  "   i
     �
     �
  H   �
  (   �
  #        >     Q  :   _  +   �  '   �  =   �     ,     H      a     �     �  �   �     Q     Y  1   y      �     �     �  y   �  %   b     �     �     �  0   �     
             "                	       $                                    '      &             #                              !       %                                            Best Regards, Cancel next scheduled scans Comments from spammers / number of comments verified Content of comments checked as spam / number of comments verified Date Deploy From spammers Hi %s! Next scan on %s days. No Report On content People with more than 2 spam comments / number of people verified Period (days) for scanning spammers SEE FULL REPORTS (%s) Scan now! Scanning... Schedule next scan for the period defined Search and destroy spams and spammers. Spam comments / total number of comments Spam comments found / number of comments verified Spaminator is deployed. Spaminator is scanning... Spaminator is withhold. Spaminator reports Spaminator settings Spaminator was never deployed. While deployed, Spaminator will periodically scan your social network for spams. Do it now! Spammers Spammers / total number of people Spammers found / number of users verified Spams Spams on comments Total We are sorry that this procedure might bother you a lot, but it's necessary for the healthy of our network. We hope to see you back soon! With no network With spam comments Withhold [%s] You must reactivate your account. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 Mit freundlichen Grüßen, Nächste Durchsuchungen stornieren Kommentare von Spammern / Zahl der nachprüfbaren Kommentare Inhalt der Kommentare auf Spam überprüft / Zahl der nachprüfbaren Kommentare Datum Installieren Von Spammern Hallo %s! Nächste Durchsuchung in %s Tagen. Kein Bericht Auf dem Inhalt Leute mit mehr als zwei Spam-Kommentaren / Zahl der nachprüfbaren Leute Intervall in Tagen, um Spammer zu suchen VOLLSTÄNDIGE BERICHTE ANSEHEN (%s) Jetzt durchsuchen! Durchsuche... Nächste Durchsuchung für die angegebene Periode ansetzen Suchen und zerstören Sie Spam und Spammer. Spam-Kommentare / Gesamtzahl Kommentare Spam-Kommentare gefunden / Zahl der nachprüfbaren Kommentare Spaminator ist installiert. Spaminator durchsucht... Spaminator wird zurückgehalten. Spaminator-Berichte Spaminator-Einstellungen Spaminator ist noch nicht installiert. Mit einer Installation wird Spaminator regelmäßig das soziale Netzwerk nach Spam durchsuchen. Installieren Sie es jetzt! Spammer Spammers / Gesamtzahl an Leuten Spammer gefunden / Zahl der nachprüfbaren Nutzer Spam (ungewünschte Nachrichten) Spam auf Kommentaren Gesamt Es tut uns leid, wenn Sie dieser Ablauf beeinträchtigt, aber er ist für den Gesundheitszustand des Netzwerks notwendig. Wir hoffen, Sie bald wieder zu sehen! Ohne Netzwerk Mit Spam-Kommentaren Zurückhalten [%s] Sie müssen Ihren Zugang erneut aktivieren. 