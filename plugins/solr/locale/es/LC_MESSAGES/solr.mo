��          �      �           	          #     8     M     ]     m     �     �     �     �     �     �     �     �            	   %     /  	   E     O     `  �  r     "     *     D     _     w     �  #   �     �     �     �     �                     6     F     \  	   s     }  
   �     �     �                                                            
                                	             cert.  %s articles found %s communities found %s enterprises found %s events found %s people found %s products offers found Closest to me In the last day In the last month In the last week In the last year Name No filter available Older than one year Published date Related products Relevance Showing page %s of %s Situation Sort results by  Type in an option Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-23 11:35+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/plugin-solr/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
  cert.  %s artículos encontrados %s comunidades encontradas %s empresas encontradas %s eventos encontrados %s personas encontrados %s ofertas de productos encontradas Más cercano a mí Ayer En el mes pasado En la semana pasada En el año pasado Nombre Sin filtro disponible Más de un año Fecha de publicación Productos relacionados Relevante Mostrando página %s de %s Situación Ordenar resultados por Teclee una opción 