��          �   %   �      P     Q     Y     k     �     �     �     �     �     �     �     �                %     9     M     \  	   m     w  	   �     �     �     �     �     �  �  �     �     �     �     �            "   2     U     j     y     �     �     �     �     �     �     �            
   *     5     M     `  
   ~     �                                          
                	                                                                    cert.  %s articles found %s communities found %s enterprises found %s events found %s people found %s products offers found Closest to me In the last day In the last month In the last week In the last year Name No filter available Older than one year Published date Related products Relevance Showing page %s of %s Situation Sort results by  Type in an option Uses Solr as search engine. facets|Enabled facets|Not enabled Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-23 11:36+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-solr/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
  cert.  %s artigos encontrados %s comunidades encontradas %s empreendimentos encontrados %s eventos encontrados %s pessoas encontradas %s ofertas de produtos encontrados Mais próximo de mim No último dia No último mês Na última semana No último ano Nome Nenhum filtro disponível Mais de um ano Data de publicação Produtos relacionados Relevância Mostrando página %d de %d Situação Ordenar resultados por  Digite uma opção Usa Solr como motor de busca. Habilitado Não habilitado 