��          �            x     y  X   �     �     �     �     �  &   �          -     2     :     J     S     e     u  �  |  	   6  _   @     �     �  	   �     �  ,   �     �  	               	   (     2     G     ^                           
                     	                               Cancel Defines a work to be done by the members and receives their submissions about this work. Delete Edit Message Name New kind of content for organizations. Publish submissions Save Subject Submission date Versions View all versions Work Assignment e-Mail Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-02-23 11:34+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/plugin-work-assignment/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Abbrechen Definiert ein Arbeit, die von den Mitgliedern erledigt werden muss, und empfängt diese Arbeit. Löschen Ändern Nachricht Name Neue Art von Beiträgen für Organisationen. Beiträge veröffentlichen Speichern Betreff Beitragsdatum Versionen Zeige alle Versionen Zuweisung von Arbeiten E-Mail 