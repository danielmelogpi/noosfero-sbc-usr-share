��          �      |      �  '   �     !     (  X   ?     �     �  {   �           (  &   -     T     j     �     �     �     �     �     �     �     �     �  �    F   �            P   9     �     �  �   �           )  +   .     Z  !   u     �     �     �     �     �     �     �     	     !                                                                	   
                                      Allow users change submissions privacy? Cancel Default email message: Defines a work to be done by the members and receives their submissions about this work. Delete Edit If you want to notify someone about this action, fill the field below with the emails of the destinies, separated by comma. Message Name New kind of content for organizations. Notification not sent Notification successfully sent Publish submissions Save Send notification to:  Subject Submission date Versions View all versions Work Assignment e-Mail Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-08-07 16:48+0200
Last-Translator: Antonio Terceiro <terceiro@softwarelivre.org>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-work-assignment/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.4-dev
 Permitir que usuários alterem o nível de privacidade de submissões? Cancelar Mensagem de e-mail padrão: Define um trabalho a ser feito pelos membros e recebe as submissõs do trabalho. Remover Editar Se você quiser notificar alguém sobre esta ação, preenha o campo abaixo com os e-mails dos destinatários, separados por vírgula. Mensagem Nome Novo tipo de conteúdo para organizações. Notificação não enviada Notificação enviada com sucesso Publicar submissões Salvar Enviar notificação para:  Assunto Data de submissão Versões Ver todas as versões Trabalho a ser entregue e-Mail 