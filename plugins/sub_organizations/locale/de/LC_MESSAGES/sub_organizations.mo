��          �      �       H  W   I  /   �     �  c   �     P     b     t  
   �     �  &   �     �  !   �  %     �  D  o   �  :   `  %   �  �   �     D     [      m     �  4   �  8   �      	  0   *  5   [                                                	             
    %{requestor} wants to add this organization as a sub-organization of %{linked_subject}. Adds the ability for groups to have sub-groups. Create a new sub-community Fill in the search field to find the groups that should be added as sub-group of this organization: Manage sub-groups Paternity request Register a new sub-enterprise Sub-groups Sub-groups awaiting approval: Sub-organizations could not be updated Sub-organizations updated Type in a search term for a group multi-level paternity is not allowed. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-12 14:23+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: German <https://hosted.weblate.org/projects/noosfero/noosfero/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 %{requestor} möchte das Unternehmen %{linked_subject} als Unterorganisation von %{linked_subject} hinzufügen. Fügt Gruppen die Fähigkeit hinzu, Untergruppen zu haben. Eine neue Untergemeinschaft erstellen Schreiben Sie etwas in das Suchfeld, um Gruppen zu finden, welche als Untergruppen dieser Organisation hinzugefügt werden sollen: Untergruppen verwalten Zuordnungsanfrage Neues Unter-Unternehmen anmelden Untergruppen Untergruppen, welche noch bestätigt werden müssen: Die Unterorganisationen konnte nicht aktualisiert werden Unterorganisationen aktualisiert Geben Sie einen Suchbegriff für eine Gruppe ein Die Zuordnung über mehrere Stufen ist nicht erlaubt. 