��          �      \      �  R   �  S   $  W   x  W   �  /   (     X     ]  c   x     �     �             
   4     ?  &   ]     �  !   �  %   �  %   �  �    R   �  Z     X   w  Z   �  4   +     `     f  m   �     �     	  $   	     D	  
   `	  "   k	  -   �	     �	  )   �	  +   
  +   1
                                   	                        
                                        "{#} #{display_type[:title]} community"  "{#} #{display_type[:title]} communities" "{#} #{display_type[:title]} enterprise"  "{#} #{display_type[:title]} enterprises" "{#} #{display_type[:title]} organization"  "{#} #{display_type[:title]} organizations" %{requestor} wants to add this organization as a sub-organization of %{linked_subject}. Adds the ability for groups to have sub-groups. Both Create a new sub-community Fill in the search field to find the groups that should be added as sub-group of this organization: Manage sub-groups Paternity request Register a new sub-enterprise Related Organizations Sub-groups Sub-groups awaiting approval: Sub-organizations could not be updated Sub-organizations updated Type in a search term for a group Type of organizations to be displayed multi-level paternity is not allowed. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 "{#} #{display_type[:title]} comunidade" "{#} #{display_type[:title]} comunidades" "{#} #{display_type[:title]} empreendimento" "{#} #{display_type[:title]} empreendimentos" "{#} #{display_type[:title]} organização" "{#} #{display_type[:title]} organizações" %{requestor} quer associar esta organização como sub-organização de %{linked_subject}. Adiciona a possibilidade de grupos terem sub-grupos. Ambos Criar nova sub-comunidade Preencha o campo de busca para encontrar grupos que devem ser adicionados como sub-grupo dessa organização: Gerenciar sub-grupos Requisição de paternidade Registrar um novo sub-empreendimento Organizações Relacionadas Sub-grupos Sub-grupos aguardando aprovação: As sub-organizações não puderam ser salvas Sub-organizações atualizadas Digite um termo de pesquisa para um grupo Tipo de organizações para serem mostradas paternidade multi-nível não é permitido. 