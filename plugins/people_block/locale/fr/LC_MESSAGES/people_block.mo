��          �            h  !   i  /   �  /   �  /   �          +     9     M     d     }     �     �  
   �     �  �  �  )   �  ;   �  8   �  ;   /     k          �     �     �  	   �     �     �                  	                                       
                                 A plugin that adds a people block Clicking a friend takes you to his/her homepage Clicking a member takes you to his/her homepage Clicking a person takes you to his/her homepage Filter by role: Random people See all suggestions Show join leave button Some suggestions for you friends|View all members {#} %s {#} People {#} friend {#} friends Project-Id-Version: 1.2~rc2-23-g29aba34
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-07-03 00:41+0200
Last-Translator: Christophe DANIEL <papaeng@gmail.com>
Language-Team: French <https://hosted.weblate.org/projects/noosfero/plugin-people-block/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.4-dev
 Un plugin qui ajoute un bloc de personnes Cliquer sur une personne vous amène à sa page personnelle Cliquer sur un membre vous amène à sa page personnelle Cliquer sur une personne vous amène à sa page personnelle Filtrer par rôle : Personnes au hasard Voir toutes les suggestions Afficher le bouton quitter Quelques suggestions pour vous Voir tout membres {#} %s {#} Personne {#} ami {#} amis 