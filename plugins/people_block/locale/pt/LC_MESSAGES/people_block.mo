��          �            h  !   i  /   �  /   �  /   �          +     9     M     d     }     �     �  
   �     �  �  �  *   t  ?   �  4   �  5        J     ]     q     �     �  	   �     �     �     �     �        	                                       
                                 A plugin that adds a people block Clicking a friend takes you to his/her homepage Clicking a member takes you to his/her homepage Clicking a person takes you to his/her homepage Filter by role: Random people See all suggestions Show join leave button Some suggestions for you friends|View all members {#} %s {#} People {#} friend {#} friends Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-03-12 14:41+0200
Last-Translator: daniel <dtygel@eita.org.br>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-people-block/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Um plugin que adiciona um bloco de pessoas Clicando em um amigo você será enviado a página inicial dele Clicando em um membro você irá para a página dele Clicando em uma pessoa você irá para a página dela Filtrar por papel: Pessoas aleatórias Veja todas as sugestões Mostrar botão entrar/sair Algumas sugestões para você Ver todos integrantes {#} %s {#} Pessoas {#} amigo {#} amigos 