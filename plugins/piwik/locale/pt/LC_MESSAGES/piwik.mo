��          T      �       �      �      �   )   �   +        2  9   @  �  z     7     I  =   i  A   �     �  8   �                                        Piwik domain Piwik plugin settings Piwik plugin settings could not be saved. Piwik plugin settings updated successfully. Piwik site id Tracking and web analytics to your Noosfero's environment Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Domínio do Piwik Configurações do plugin Piwik As configurações do plugin Piwik não puderam ser gravadas. As configurações do plugin Piwik foram atualizadas com sucesso. Id do site do Piwik Rastreamento e análise web para o seu ambiente Noosfero 