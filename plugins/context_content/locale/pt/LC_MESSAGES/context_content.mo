��    	      d      �       �   4   �           -     E     X  *   j  -   �     �  �  �  >   �     �     �     �       7   6  3   n     �                	                              A plugin that display content based on page context. Display content types: Display context content Show content image Show content name Show parent content when children is empty This block displays content based on context. more Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Um plugin que mostra conteúdo baseado do contexto da página. Mostrar tipos de conteúdos: Mostrar conteúdo contextual Mostrar imagem do conteúdo Mostrar o nome do conteúdo Mostrar conteúdo do pai quando os filhos estão vazios Este bloco apresenta conteúdo baseado no contexto. mais 