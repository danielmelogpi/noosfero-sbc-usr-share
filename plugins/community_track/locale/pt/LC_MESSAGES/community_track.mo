��    %      D  5   l      @     A  	   H     R     b     s     �     �     �     �  	   �  $   �     �     �  
   �     �               =     W     \      a     �  <   �  3   �     �  	   �                 
   %     0     7     <     E  "   J     m  �  �     ?     G     P     b     u     �     �     �     �     �  (   �     �     �               (  (   >     g     �     �     �     �  E   �  9   �  
   7	     B	     U	     b	     i	     �	     �	     �	     �	     �	  *   �	     �	             %            #                                                                    
              $                   !                                           "   	       Closed Create %s Defines a step. Defines a track. Expected Results: Goals: Hidden Step Hidden Steps Join! New Track New kind of content for communities. Period Reorder Steps Save Order Select Categories Select Community Select one community to proceed Show more at another page Soon Step Step not allowed at this parent. Steps This block displays a list of most relevant tracks as cards. This block displays a list of most relevant tracks. Tool Tool type Tool:  Track Track Card List Track List Tracks View View All hits must be equal or after start date. should not be blank. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-12-18 18:40-0200
Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/noosfero/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0
 Fechado Criar %s Define uma etapa. Define uma trilha. Resultados Esperados: Metas: Passo Oculto Passos Ocultos Entrar! Nova Trilha Novo tipo de conteúdo para comunidades. Período Reordenar Passos Salvar Ordem Selecionar Categorias Selecionar Comunidade Selecione uma comunidade para prosseguir Mostrar mais em outra página Logo Passo Passo não permitido nesse pai. Passos Este bloco apresenta a lista das trilhas mais relevantes como cartas. Este bloco apresenta a lista das trilhas mais relevantes. Ferramenta Tipo de ferramenta Ferramenta:  Trilha Lista de Cartas da Trilha Lista de Trilhas Trilhas Ver Ver Todos(as) acessos deve ser igual ou após a data de ínicio. não deve ser nulo. 