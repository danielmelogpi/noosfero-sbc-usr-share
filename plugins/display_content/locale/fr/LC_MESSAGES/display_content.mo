��          �      |      �     �          ,     <  U   T     �     �     �  E   �  )      -   J     x     �     �     �     �  	   �     �  	   �  �   �     p  �  u     I     ^     {     �  ^   �               '  ^   ;  #   �  K   �     
     (     >     G     V     f     r  	   �  �   �     	                                                                 
   	                                     %{month_name} %{day} %{month_name} %{day}, %{year} %{month}/%{day} %{month}/%{day}/%{year} A plugin that adds a block where you could choose any of your content and display it. Abstract Choose by Content Type Choose directly Choose which attributes should be displayed and drag to reorder them: Choose which content should be displayed: Dinamically load children of selected folders Display content types: Display your contents Limit: Most oldest Most recent Order by: Publish date Read more This block displays articles chosen by you. You can edit the block to select which of your articles is going to be displayed in the block. more Project-Id-Version: 1.2~rc2-23-g29aba34
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-07-03 00:31+0200
Last-Translator: Christophe DANIEL <papaeng@gmail.com>
Language-Team: French <https://hosted.weblate.org/projects/noosfero/plugin-display-content/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.4-dev
 %{day} %{month_name} %{day} %{month_name} %{year} %{day}/%{month} %{day}/%{month}/%{year} Un plugin qui permet d'ajouter une zone ou vous pourrez y afficher le contenue de votre choix. Résumé Choisir par type de contenu Choisir directement Choisissez quels attribues doivent êtres afficher et glisser/déposer pour les réorganiser : Choisissez le contenu à afficher : charger dynamiquement les sous répertoires des répertoires sélectionnés Afficher le type de contenu : Afficher vos contenus Limite : Le plus ancien Le plus récent Trier par : Date de publication Lire plus Ce bloc affiche l'article de votre choix. Vous pouvez éditer le bloc pour sélectionner lequel de vos articles va être affiche dans ce bloc. plus 