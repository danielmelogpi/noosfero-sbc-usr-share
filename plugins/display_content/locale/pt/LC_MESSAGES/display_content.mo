��          �      |      �     �          ,     <  U   T     �     �     �  E   �  )      -   J     x     �     �     �     �  	   �     �  	   �  �   �     p  �  u     -  "   E     h     x  c   �     �     �       J   .  )   y  5   �     �     �               %     3     @  	   U  �   _     �                                                                 
   	                                     %{month_name} %{day} %{month_name} %{day}, %{year} %{month}/%{day} %{month}/%{day}/%{year} A plugin that adds a block where you could choose any of your content and display it. Abstract Choose by Content Type Choose directly Choose which attributes should be displayed and drag to reorder them: Choose which content should be displayed: Dinamically load children of selected folders Display content types: Display your contents Limit: Most oldest Most recent Order by: Publish date Read more This block displays articles chosen by you. You can edit the block to select which of your articles is going to be displayed in the block. more Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2015-03-12 15:14+0200
Last-Translator: daniel <dtygel@eita.org.br>
Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/plugin-display-content/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 %{day} de %{month_name} %{day} de %{month_name} de %{year} %{day}/%{month} %{day}/%{month}/%{year} Um plugin que adiciona blocos onde você pode escolher qualuer um dos seus conteúdos e mostrá-lo. Resumo Escolha pelo Tipo de conteúdo Escolha diretamente Escolha quais atributos devem ser mostrados e mova-os parar reordená-los: Escolha qual conteúdo deve ser mostrado: Carregar dinamicamente filhos das pastas selecionadas Mostrar tipos de conteúdos: Apresente seus conteúdos Limite: Mais antigas Mais recentes Ordenar por: Data de publicação Leia mais Este bloco apresenta artigos escolhidos por você. Você pode editar o bloco para escolher qual dos seus artigos será apresentado nele. mais 