��    ,      |  ;   �      �     �  9   �  >   !  6   `     �     �     �     �     �     �     �     �  	   �     �     �               "     +     4     <     A     F     K     Q  
   U     `     w     �     �     �     �  	   �  #   �     �  	   �               *  !   @  J   b  �   �  ?   2  �  r  $   -	  4   R	  ;   �	  4   �	     �	     
     &
     :
     =
     B
     F
     J
     N
     V
     ]
     p
     y
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          $     A      J     k  	   w     �     �     �     �  E   �  �     =   �     *         "            ,                   !           '                 $          )             #   (                      +   	      
   %                                                        &       %{month_name} %{day}, %{year} %{requestor} commented on the article: %{linked_subject}. %{requestor} wanted to comment the article but it was removed. %{requestor} wants to comment the article: %{article}. &laquo; Newer posts (removed user) (unauthenticated user) 0% 100% 25% 50% 75% Anonymous April Article removed. August Community invitation December February January Join July June March May My network New comment to article No comments yet November October Older posts &raquo; Products/Enterprises search Read more Search for enterprises and products See all September Sign up. The article was removed. Uncategorized product View comment View all %s comments You need to login on %{system} in order to approve or reject this comment. Your comment to the article "%{article}" was approved. Here is the comment left by the admin who approved your comment:

%{comment}  Your request for comment the article "%{article}" was approved. Project-Id-Version: 1.2~rc2-23-g29aba34
POT-Creation-Date: 2015-08-06 18:47-0300
PO-Revision-Date: 2014-11-05 13:05+0200
Last-Translator: Aurélio A. Heckert <aurelio@colivre.coop.br>
Language-Team: Esperanto <https://hosted.weblate.org/projects/noosfero/noosfero/eo/>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.0-dev
 La %{day}-a de %{month_name} %{year} %{requestor} komentarii artikolo: %{linked_subject}. %{requestor} volis komenti la artikolon sed estis forigita. %{requestor} volas komenti la artikolon: %{article}. &laquo; Pli freŝaj afiŝoj (forigita uzanto) (malverigis uzanto) 0% 100% 25% 50% 75% Anonima Aprilo Artikolo forigita. Aŭgusto komunumo invito Decembro Februaro Januaro Eniri Julio Junio Marto Majo Mia reto Novaj komentoj al la artikolo Neniu komento Novembro Oktobro Malpli freŝaj afiŝoj &raquo Produktoj/Entreprenoj serĉo Legi pli Serĉu entreprenoj kaj produktoj Vidi ĉiujn Septembro Aliĝi La artikolo estis forigita. Neenkategoriitaj produkto Unu komento Unu komento Vi devas eniri sur %{system} por aprobi aŭ malakcepti tiun komenton. Via komento al la artikolo "%{article}" estis aprobita. Jen la komento lasita de la administristo, kiu aprobis vian komenton:

%{comment}  Via peto por komento la artikolo "%{article}" estis aprobita. 