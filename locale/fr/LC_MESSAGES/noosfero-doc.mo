��          �      �       8     9     E  u   M     �     �     �     �               "     *     1  �  7     �  	   �  {        }     �     �  %   �     �                                     	                             
                     Add a block Catalog Create a <span class="caps">HTML</span> form using RawHTMLBlock or RawHTMLArticle that invokes the {sendemail} action Description Edit appearance Edit sideboxes Editing header and footer Editing sideboxes Options Plugins Search Usage Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2013-12-10 15:48-0300
PO-Revision-Date: 2015-06-29 22:07+0200
Last-Translator: Christophe DANIEL <papaeng@gmail.com>
Language-Team: French <https://hosted.weblate.org/projects/noosfero/documentation/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 2.4-dev
 Ajouter un bloc Catalogue Créer un formulaire <span class="caps">HTML</span> utilisant RawHTMLBlock ou RawHTMLArticle qui appel l'action {sendemail} Description Éditer l'apparence Éditer les boîtes de menus Édition des en-tête et pied de page Édition des boîtes latérales Options Plugins Chercher Utilisation 